const connectLivereload = require('connect-livereload')
const livereload = require('livereload')
const log = require("@ui5/logger").getLogger("middleware:custom:livereload")
const path = require("path")



/**
 * Custom UI5 Server middleware example
 *
 * @param {Object} parameters Parameters
 * @param {Object} parameters.resources Resource collections
 * @param {module:@ui5/fs.AbstractReader} parameters.resources.all Reader or Collection to read resources of the
 *                                        root project and its dependencies
 * @param {module:@ui5/fs.AbstractReader} parameters.resources.rootProject Reader or Collection to read resources of
 *                                        the project the server is started in
 * @param {module:@ui5/fs.AbstractReader} parameters.resources.dependencies Reader or Collection to read resources of
 *                                        the projects dependencies
 * @param {Object} parameters.options Options
 * @param {string} [parameters.options.configuration] Custom server middleware configuration if given in ui5.yaml
 * @returns {function} Middleware function to use
 */
module.exports = function({resources, options}) {
    const config = {...{
        path: 'webapp',
        port: 35729,
        extraExts: "xml,json,properties"
    }, ...options.configuration }
    
    const livereloadServer = livereload.createServer(config, () => {
        log.info("Livereload server started...")
    })
    options.configuration.debug ? log.info(`Livereload connecting to port ${config.port} for path ${config.path}`) : null;
    
    // support to watch multiple directories
    if(!Array.isArray(config.path)){
        config.path = [config.path]
    }
    config.path.forEach(element => {
        livereloadServer.watch(path.join(process.cwd(), element))
    })
 
    return connectLivereload({
        port: config.port
    })
}
